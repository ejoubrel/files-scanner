package fr.ensai.filescanner.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import fr.ensai.filescanner.dto.ScanInDto;
import fr.ensai.filescanner.dto.ScanOutDto;
import fr.ensai.filescanner.dto.ScanOutFullDto;
import fr.ensai.filescanner.dto.ScanParametersDto;
import fr.ensai.filescanner.service.ScanService;

import java.util.Arrays;
import java.util.List;

@WebMvcTest(ScanController.class)
public class ScanControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ScanService scanService;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        // Initialisation des données de mock si nécessaire
    }

    
    /** 
     * @throws Exception
     */
    @Test
    public void testGetScans() throws Exception {
        List<ScanOutDto> scans = Arrays.asList(new ScanOutDto(null, null, null, null, null, null, null, null), new ScanOutDto(null, null, null, null, null, null, null, null));
        when(scanService.getScans()).thenReturn(scans);

        mockMvc.perform(get("/scans"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    
    /** 
     * @throws Exception
     */
    @Test
    public void testGetScan() throws Exception {
        long id = 1L;
        ScanOutFullDto scan = new ScanOutFullDto(id, null, null, null, null, null, null, id, null);
        when(scanService.getScan(id)).thenReturn(scan);

        mockMvc.perform(get("/scans/{id}", id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    
    /** 
     * @throws Exception
     */
    @Test
    public void testNewScan() throws Exception {
        ScanInDto scanInDto = new ScanInDto(null, null, null, null, null, null); // Set properties accordingly
        ScanOutFullDto scanOutFullDto = new ScanOutFullDto(null, null, null, null, null, null, null, null, null); // Set properties accordingly
        when(scanService.saveScan(any(ScanInDto.class))).thenReturn(scanOutFullDto);

        mockMvc.perform(post("/scans")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(scanInDto)))
                .andExpect(status().isCreated());
    }

    
    /** 
     * @throws Exception
     */
    @Test
    public void testDeleteScan() throws Exception {
        long id = 1L;
        when(scanService.deleteScan(id)).thenReturn(true); // Simulez que le scan a été trouvé et supprimé
    
        mockMvc.perform(delete("/scans/{id}", id))
                .andExpect(status().isOk());
    
        verify(scanService, times(1)).deleteScan(id); // Vérifiez que la méthode a été appelée
    }

    
    /** 
     * @throws Exception
     */
    @Test
    public void testDuplicateScan() throws Exception {
        long id = 1L;
        ScanParametersDto scanParametersDto = new ScanParametersDto(); // Configurez selon vos besoins
        ScanOutFullDto duplicatedScan = new ScanOutFullDto(id, null, null, null, null, null, null, id, null); // Configurez le DTO retourné selon vos besoins

        when(scanService.duplicateScan(eq(id), any(ScanParametersDto.class))).thenReturn(duplicatedScan);

        mockMvc.perform(post("/scans/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(scanParametersDto)))
                .andExpect(status().isCreated());
    }

}
