package fr.ensai.filescanner;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import fr.ensai.filescanner.service.ScanService;

@SpringBootTest
@ContextConfiguration(classes = Application.class)
public class ApplicationTests {

    @Autowired
    private ScanService scanService;

    @Test
    public void contextLoads() {
        assertNotNull(scanService);
    }
}
