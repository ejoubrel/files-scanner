package fr.ensai.filescanner.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDateTime;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

public class FileInfoTest {

    @Test
    public void testExtractFileType() {
        FileInfo fileInfo = new FileInfo("example.txt", LocalDateTime.now(), 1024L, "/path/to/file");
        assertEquals(".txt", fileInfo.getFileType(), "Le type de fichier extrait doit correspondre à l'extension du fichier");
    }

    @Test
    public void testConstructorSetsFieldsCorrectly() {
        LocalDateTime modificationDate = LocalDateTime.now();
        FileInfo fileInfo = new FileInfo("example.txt", modificationDate, 1024L, "/path/to/file");

        assertEquals("example.txt", fileInfo.getFileName());
        assertEquals(modificationDate, fileInfo.getModificationDate());
        assertEquals(1024L, fileInfo.getFileSize());
        assertEquals("/path/to/file", fileInfo.getParentDirectory());
        assertEquals(".txt", fileInfo.getFileType());
    }

    @Test
    public void testGetterAndSetter() {
        FileInfo fileInfo = new FileInfo();
        fileInfo.setId(1L);
        fileInfo.setFileName("test.txt");
        fileInfo.setModificationDate(LocalDateTime.now());
        fileInfo.setFileSize(1024L);
        fileInfo.setFileType(".txt");
        fileInfo.setParentDirectory("/path/to/directory");
        fileInfo.setScans(new HashSet<>());

        assertEquals(1L, fileInfo.getId());
        assertEquals("test.txt", fileInfo.getFileName());
        assertNotNull(fileInfo.getModificationDate());
        assertEquals(1024L, fileInfo.getFileSize());
        assertEquals(".txt", fileInfo.getFileType());
        assertEquals("/path/to/directory", fileInfo.getParentDirectory());
        assertNotNull(fileInfo.getScans());
    }

    @Test
    public void testEqualsAndHashCode() {
        FileInfo fileInfo1 = new FileInfo();
        fileInfo1.setId(1L);
        FileInfo fileInfo2 = new FileInfo();
        fileInfo2.setId(1L);
        FileInfo fileInfo3 = new FileInfo();
        fileInfo3.setId(2L);

        assertEquals(fileInfo1, fileInfo2);
        assertNotEquals(fileInfo1, fileInfo3);
        assertEquals(fileInfo1.hashCode(), fileInfo2.hashCode());
        assertNotEquals(fileInfo1.hashCode(), fileInfo3.hashCode());
    }

    @Test
    public void testToString() {
        FileInfo fileInfo = new FileInfo();
        fileInfo.setId(1L);
        fileInfo.setFileName("test.txt");
        fileInfo.setModificationDate(LocalDateTime.of(2022, 1, 1, 10, 30));
        fileInfo.setFileSize(1024L);
        fileInfo.setFileType(".txt");
        fileInfo.setParentDirectory("/path/to/directory");
        fileInfo.setScans(new HashSet<>());

        String expectedToString = "FileInfo(id=1, fileName=test.txt, modificationDate=2022-01-01T10:30, fileSize=1024, fileType=.txt, parentDirectory=/path/to/directory, scans=[])";
        assertEquals(expectedToString, fileInfo.toString());
    }
}
