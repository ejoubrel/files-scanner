package fr.ensai.filescanner.model;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import fr.ensai.filescanner.scanner.ScannerStrategyName;

public class ScanTest {

    @Test
    public void testDefaultConstructor() {
        Scan scan = new Scan();

        assertNull(scan.getId());
        assertEquals(LocalDate.now(), scan.getScanDate());
        assertNull(scan.getScanTime());
        assertTrue(scan.getFiles().isEmpty());
    }

    @Test
    public void testScanConstructorWithParameters() {
        String directoryPath = "/path/to/directory";
        Integer maxFileCount = 100;
        Integer maxDepth = 5;
        String fileNameFilter = "*.txt";
        String fileTypeFilter = ".txt";
        ScannerStrategyName strategyName = ScannerStrategyName.Local;

        Scan scan = new Scan(directoryPath, maxFileCount, maxDepth, fileNameFilter, fileTypeFilter, strategyName);

        assertEquals(directoryPath, scan.getDirectoryPath());
        assertEquals(maxFileCount, scan.getMaxFileCount());
        assertEquals(maxDepth, scan.getMaxDepth());
        assertEquals(fileNameFilter, scan.getFileNameFilter());
        assertEquals(fileTypeFilter, scan.getFileTypeFilter());
        assertEquals(strategyName, scan.getStrategyName());
        assertEquals(LocalDate.now(), scan.getScanDate());
        assertNull(scan.getScanTime()); // Assuming scanTime is set later
        assertTrue(scan.getFiles().isEmpty()); // Assuming files are added later
    }

    // Test du constructeur avec ID
    @Test
    public void testScanConstructorWithId() {
        Long id = 1L;
        String directoryPath = "/another/path";
        Integer maxFileCount = 50;
        Integer maxDepth = 3;
        String fileNameFilter = "*.jpg";
        String fileTypeFilter = ".jpg";
        ScannerStrategyName strategyName = ScannerStrategyName.S3Bucket;

        Scan scan = new Scan(id, directoryPath, maxFileCount, maxDepth, fileNameFilter, fileTypeFilter, strategyName);

        assertEquals(id, scan.getId());
        assertEquals(directoryPath, scan.getDirectoryPath());
        // Répétez les assertions pour les autres champs comme dans le premier test
    }

    @Test
    public void testConstructorWithId() {
        Long id = 1L;
        String directoryPath = "/path/to/directory";
        Integer maxFileCount = 100;
        Integer maxDepth = 5;
        String fileNameFilter = "*.txt";
        String fileTypeFilter = ".txt";
        ScannerStrategyName strategyName = ScannerStrategyName.Local;

        Scan scan = new Scan(id, directoryPath, maxFileCount, maxDepth, fileNameFilter, fileTypeFilter, strategyName);

        assertEquals(id, scan.getId());
        assertEquals(directoryPath, scan.getDirectoryPath());
        assertEquals(maxFileCount, scan.getMaxFileCount());
        assertEquals(maxDepth, scan.getMaxDepth());
        assertEquals(fileNameFilter, scan.getFileNameFilter());
        assertEquals(fileTypeFilter, scan.getFileTypeFilter());
        assertEquals(strategyName, scan.getStrategyName());
        assertEquals(LocalDate.now(), scan.getScanDate());
        assertNull(scan.getScanTime());
        assertTrue(scan.getFiles().isEmpty());
    }

    @Test
    public void testConstructorWithoutId() {
        String directoryPath = "/path/to/directory";
        Integer maxFileCount = 100;
        Integer maxDepth = 5;
        String fileNameFilter = "*.txt";
        String fileTypeFilter = ".txt";
        ScannerStrategyName strategyName = ScannerStrategyName.Local;

        Scan scan = new Scan(directoryPath, maxFileCount, maxDepth, fileNameFilter, fileTypeFilter, strategyName);

        assertNull(scan.getId());
        assertEquals(directoryPath, scan.getDirectoryPath());
        assertEquals(maxFileCount, scan.getMaxFileCount());
        assertEquals(maxDepth, scan.getMaxDepth());
        assertEquals(fileNameFilter, scan.getFileNameFilter());
        assertEquals(fileTypeFilter, scan.getFileTypeFilter());
        assertEquals(strategyName, scan.getStrategyName());
        assertEquals(LocalDate.now(), scan.getScanDate());
        assertNull(scan.getScanTime());
        assertTrue(scan.getFiles().isEmpty());
    }

    // Test de l'ajout de fichiers
    @Test
    public void testAddingFiles() {
        Scan scan = new Scan();
        FileInfo fileInfo1 = new FileInfo("file1.txt", LocalDateTime.now(), 100L, "/path/to/file1");
        FileInfo fileInfo2 = new FileInfo("file2.txt", LocalDateTime.now(), 200L, "/path/to/file2");

        scan.getFiles().add(fileInfo1);
        scan.getFiles().add(fileInfo2);

        assertEquals(2, scan.getFiles().size());
    }

    // Test des valeurs nulles dans le constructeur
    @Test
    public void testScanConstructorWithNullValues() {
        Scan scan = new Scan(null, null, null, null, null, null);

        assertNull(scan.getDirectoryPath());
        assertNull(scan.getMaxFileCount());
        assertNull(scan.getMaxDepth());
        assertNull(scan.getFileNameFilter());
        assertNull(scan.getFileTypeFilter());
        assertNotNull(scan.getScanDate());
        assertNull(scan.getScanTime());
        assertTrue(scan.getFiles().isEmpty());
    }

    // Test de la logique métier spécifique (si applicable)
    // Par exemple, mettre à jour le scanTime et vérifier que la modification est appliquée correctement.
    @Test
    public void testUpdateScanTime() {
        Scan scan = new Scan();
        Long expectedScanTime = 5000L; // Exemple de temps d'exécution du scan
        scan.setScanTime(expectedScanTime);

        assertEquals(expectedScanTime, scan.getScanTime());
    }

    @Test
        public void testAllArgsConstructor() {
            Long id = 1L;
            String directoryPath = "/path/to/directory";
            Integer maxFileCount = 100;
            Integer maxDepth = 5;
            String fileNameFilter = "*.txt";
            String fileTypeFilter = ".txt";
            ScannerStrategyName strategyName = ScannerStrategyName.Local;
            Set<FileInfo> files = new HashSet<>();

            Scan scan = new Scan(id, maxFileCount, maxDepth, fileNameFilter, fileTypeFilter, LocalDate.now(), null, directoryPath, files, strategyName);

            assertEquals(id, scan.getId());
            assertEquals(directoryPath, scan.getDirectoryPath());
            assertEquals(maxFileCount, scan.getMaxFileCount());
            assertEquals(maxDepth, scan.getMaxDepth());
            assertEquals(fileNameFilter, scan.getFileNameFilter());
            assertEquals(fileTypeFilter, scan.getFileTypeFilter());
            assertEquals(LocalDate.now(), scan.getScanDate());
            assertNull(scan.getScanTime());
            assertEquals(directoryPath, scan.getDirectoryPath());
            assertEquals(files, scan.getFiles());
            assertEquals(strategyName, scan.getStrategyName());
        }

        @Test
        public void testSetterAndGetterForScanTime() {
            Scan scan = new Scan();
            Long expectedScanTime = 5000L;

            scan.setScanTime(expectedScanTime);

            assertEquals(expectedScanTime, scan.getScanTime());
        }

    @Test
    public void testEquals() {
        // Création de deux objets Scan avec les mêmes attributs
        Scan scan1 = new Scan(1L, "/path", 100, 5, "*.txt", ".txt", null);
        Scan scan2 = new Scan(1L, "/path", 100, 5, "*.txt", ".txt", null);

        // Vérification que les deux objets sont égaux
        assertTrue(scan1.equals(scan2));
    }
}
