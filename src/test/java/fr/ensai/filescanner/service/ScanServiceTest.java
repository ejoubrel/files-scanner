package fr.ensai.filescanner.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.util.Optional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.server.ResponseStatusException;


import fr.ensai.filescanner.dto.*;
import fr.ensai.filescanner.model.*;
import fr.ensai.filescanner.repository.*;
import fr.ensai.filescanner.scanner.*;

@SpringBootTest
public class ScanServiceTest {

    @Mock
    private ScanRepository scanRepository;

    @Mock
    private FileInfoRepository fileInfoRepository;

    @Mock
    private ScannerStrategyFactory strategyFactory;

    @InjectMocks
    private ScanService scanService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        ScannerStrategy scannerStrategy = mock(ScannerStrategy.class);
        when(strategyFactory.findStrategy(any())).thenReturn(scannerStrategy);
        when(scannerStrategy.scanDirectory(anyString(), anyString(), anyString(), anyInt(), anyInt())).thenReturn(new ArrayList<>());
    }


/*  @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(strategyFactory.findStrategy(any())).thenReturn(scannerStrategy);
        when(scannerStrategy.scanDirectory(anyString(), anyString(), anyString(), anyInt(), anyInt())).thenReturn(new ArrayList<>());
    } */

    @Test
    void testGetScanFound() {
        Long id = 1L;
        Scan mockScan = new Scan();
        mockScan.setId(id); // Assurez-vous que l'ID est défini si votre classe Scan a un setter pour l'ID
        when(scanRepository.findById(id)).thenReturn(Optional.of(mockScan));

        ScanOutFullDto result = scanService.getScan(id);

        assertNotNull(result);
        assertEquals(id, result.getId()); // Assurez-vous que ScanOutFullDto.getScanId() renvoie la valeur correcte
    }

    @Test
    void testGetScanNotFound() {
        Long id = 1L;
        when(scanRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> scanService.getScan(id));
    }

    @Test
    void testDeleteScan() {
        Long id = 1L;
        when(scanRepository.findById(id)).thenReturn(Optional.of(new Scan()));

        boolean result = scanService.deleteScan(id);

        assertTrue(result);
        verify(scanRepository).deleteById(id);
    }

    @Test
    void testSaveScan() {
        ScanInDto inDto = new ScanInDto("path", ScannerStrategyName.Local, 10, 5, "filter", ".txt");
        Scan scan = new Scan();
        when(scanRepository.save(any(Scan.class))).thenReturn(scan);

        ScanOutFullDto result = scanService.saveScan(inDto);

        assertNotNull(result);
        // Add more assertions as needed based on the expected behavior
    }

    @Test
    void testDuplicateScan() {
        Long id = 1L;
        Scan initialScan = new Scan();
        ScanParametersDto params = new ScanParametersDto();
        when(scanRepository.findById(id)).thenReturn(Optional.of(initialScan));
        when(scanRepository.save(any(Scan.class))).thenReturn(initialScan);

        ScanOutFullDto result = scanService.duplicateScan(id, params);

        assertNotNull(result);
        // Add more assertions as needed based on the expected behavior
    }

    @Test
    void testReplayScan() {
        Long id = 1L;
        Scan initialScan = new Scan();
        when(scanRepository.findById(id)).thenReturn(Optional.of(initialScan));
        when(scanRepository.save(any(Scan.class))).thenReturn(initialScan);

        ScanOutFullDto result = scanService.replayScan(id);

        assertNotNull(result);
        // Add more assertions as needed based on the expected behavior
    }

    @Test
    void testComputeMeanTimePerFile() {
        // Assurez-vous de définir une valeur pour scanTime dans vos objets Scan mockés
        Scan scan = new Scan();
        scan.setScanTime(100L); // Utilisez une valeur de temps de scan non nulle
        List<Scan> scans = Collections.singletonList(scan);
        when(scanRepository.findAll()).thenReturn(scans);

        FileInfo fileInfo = new FileInfo();
        fileInfo.setId(1L); // Assurez-vous que FileInfo a un ID non nul si c'est nécessaire
        List<FileInfo> fileInfoList = Collections.singletonList(fileInfo);
        when(fileInfoRepository.findAll()).thenReturn(fileInfoList);

        double result = scanService.computeMeanTimePerFile();
        assertTrue(result >= 0);
    }

    @Test
    void testComputeMeanTimePerDir() {
        // Assurez-vous de définir une valeur pour scanTime dans vos objets Scan mockés
        Scan scan = new Scan();
        scan.setScanTime(100L); // Utilisez une valeur de temps de scan non nulle
        List<Scan> scans = Collections.singletonList(scan);
        when(scanRepository.findAll()).thenReturn(scans);

        FileInfo fileInfo = new FileInfo();
        fileInfo.setParentDirectory("someDirectory"); // Assurez-vous de définir un répertoire parent non nul
        List<FileInfo> fileInfoList = Collections.singletonList(fileInfo);
        when(fileInfoRepository.findAll()).thenReturn(fileInfoList);

        double result = scanService.computeMeanTimePerDir();
        assertTrue(result >= 0);
    }
}
