package fr.ensai.filescanner.scanner;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class ScannerStrategyFactoryTest {

    @Autowired
    private ScannerStrategyFactory scannerStrategyFactory;

    // Supposons que ces mocks soient vos stratégies concrètes
    @MockBean
    private LocalStrategy localStrategy;
    @MockBean
    private S3BucketStrategy s3BucketStrategy;

    @BeforeEach
    void setUp() {
        Set<ScannerStrategy> strategySet = new HashSet<>();
        when(localStrategy.getStrategyName()).thenReturn(ScannerStrategyName.Local);
        when(s3BucketStrategy.getStrategyName()).thenReturn(ScannerStrategyName.S3Bucket);
        strategySet.add(localStrategy);
        strategySet.add(s3BucketStrategy);
        scannerStrategyFactory = new ScannerStrategyFactory(strategySet);
    }

    @Test
    public void testFindStrategyLocal() {
        ScannerStrategy strategy = scannerStrategyFactory.findStrategy(ScannerStrategyName.Local);
        assertNotNull(strategy, "La stratégie LOCAL ne devrait pas être nulle");
        assertTrue(strategy instanceof LocalStrategy, "La stratégie devrait être une instance de LocalStrategy");
    }

    @Test
    public void testFindStrategyS3Bucket() {
        ScannerStrategy strategy = scannerStrategyFactory.findStrategy(ScannerStrategyName.S3Bucket);
        assertNotNull(strategy, "La stratégie S3_BUCKET ne devrait pas être nulle");
        assertTrue(strategy instanceof S3BucketStrategy, "La stratégie devrait être une instance de S3BucketStrategy");
    }
}
