package fr.ensai.filescanner.scanner;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.ensai.filescanner.model.FileInfo;

public class LocalStrategyTest {

    private Path tempDirectory;

    
    /** 
     * @throws IOException
     */
    @BeforeEach
    void setUp() throws IOException {
        // Créer un répertoire temporaire avec des fichiers pour le test
        tempDirectory = Files.createTempDirectory("testDirectory");
        
        // Ajouter ici la logique pour créer des fichiers et sous-répertoires dans tempDirectory
        Files.createFile(tempDirectory.resolve("testFile.txt")); // Crée un fichier testFile.txt dans le répertoire temporaire
    }
    

    
    /** 
     * @throws IOException
     */
    @AfterEach
    void tearDown() throws IOException {
        // Nettoyer le répertoire temporaire après chaque test
        Files.walk(tempDirectory)
            .map(Path::toFile)
            .forEach(File::delete);
    }

    @Test
    void testScanDirectory() {
        LocalStrategy localStrategy = new LocalStrategy();

        // Appeler scanDirectory avec le chemin de tempDirectory et les filtres désirés
        String directoryPath = tempDirectory.toString();
        List<FileInfo> files = localStrategy.scanDirectory(directoryPath, null, ".txt", 2, 10);

        // Assertions pour vérifier que les fichiers retournés correspondent aux critères
        assertFalse(files.isEmpty(), "La liste des fichiers ne devrait pas être vide");
        // Ajoutez plus d'assertions ici pour valider la logique de filtrage, etc.
    }
}
