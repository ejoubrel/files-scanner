package fr.ensai.filescanner.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.ensai.filescanner.dto.ScanInDto;
import fr.ensai.filescanner.dto.ScanOutDto;
import fr.ensai.filescanner.dto.ScanOutFullDto;
import fr.ensai.filescanner.dto.ScanParametersDto;
import fr.ensai.filescanner.service.ScanService;
import jakarta.validation.Valid;

@RestController
public class ScanController {
    
    @Autowired
    public ScanService scanService;

    
    /** 
     * @return Iterable<ScanOutDto>
     */
    @GetMapping("/scans")
    public Iterable<ScanOutDto> getScans() {
        return scanService.getScans();
    }

    
    /** 
     * @param id
     * @return ScanOutFullDto
     */
    @GetMapping("/scans/{id}")
    public ScanOutFullDto getScan(@PathVariable long id) {
        return scanService.getScan(id);

    }

    
    /** 
     * @param scan
     * @return ResponseEntity<ScanOutFullDto>
     */
    @PostMapping("/scans")
    public ResponseEntity<ScanOutFullDto> newScan(@Valid @RequestBody ScanInDto scan) {
        ScanOutFullDto newScan = scanService.saveScan(scan);
        return new ResponseEntity<ScanOutFullDto>(newScan, HttpStatus.CREATED);
    }

    
    /** 
     * @param id
     * @return ResponseEntity<String>
     */
    @DeleteMapping("/scans/{id}")
    public ResponseEntity<String> deleteScan(@PathVariable long id){
        boolean found = scanService.deleteScan(id);
        if (!found){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<String>("Deleted scan with id " + id, HttpStatus.OK);
    }

    
    /** 
     * @param duplicateScan(
     * @return ResponseEntity<ScanOutFullDto>
     */
    @PostMapping("/scans/{id}")
    public ResponseEntity<ScanOutFullDto> duplicateScan(
            @PathVariable long id,
            @RequestBody ScanParametersDto scanParameters){
        ScanOutFullDto savedScan = scanService.duplicateScan(id, scanParameters);

        return new ResponseEntity<ScanOutFullDto>(savedScan, HttpStatus.CREATED);
    }

    
    /** 
     * @param id
     * @return ResponseEntity<ScanOutFullDto>
     */
    @PatchMapping("/scans/{id}")
    public ResponseEntity<ScanOutFullDto> replayScan(@PathVariable long id) {
        ScanOutFullDto savedScan = scanService.replayScan(id);
        return new ResponseEntity<ScanOutFullDto>(savedScan, HttpStatus.OK);
    }

    
    /** 
     * @param firstScanId
     * @param secondScanId
     * @return Map<String, Double>
     */
    @GetMapping("scans/statistics")
    public Map<String, Double> computeStatistics(@RequestParam Long firstScanId, @RequestParam Long secondScanId){
        Map<String, Double> result = new HashMap<>();
        result.put("mean_time_per_file", scanService.computeMeanTimePerFile());
        result.put("mean_time_per_directory", scanService.computeMeanTimePerDir());
        return result;
    }
}
