package fr.ensai.filescanner.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

import java.util.HashSet;
import java.util.Set;


import fr.ensai.filescanner.scanner.ScannerStrategyName;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Entity
@Table(name = "scans")
@Data
@AllArgsConstructor
public class Scan {

    public Scan(){
        this.id = null;
        this.scanDate = LocalDate.now();
        this.scanTime = null;
        this.files = new HashSet<>();
    }

    public Scan(Long id, String directoryPath,
                Integer maxFileCount, Integer maxDepth,
                String fileNameFilter, String fileTypeFilter, ScannerStrategyName strategyName){
        this.id = id;
        this.maxFileCount = maxFileCount;
        this.maxDepth = maxDepth;
        this.fileNameFilter = fileNameFilter;
        this.fileTypeFilter = fileTypeFilter;
        this.scanDate = LocalDate.now();
        this.scanTime = null;
        this.files = new HashSet<>();
        this.directoryPath = directoryPath;    
        this.strategyName = strategyName;
    }
    
    public Scan(String directoryPath,
                Integer maxFileCount, Integer maxDepth,
                String fileNameFilter, String fileTypeFilter, ScannerStrategyName strategyName){
        this.id = null;
        this.maxFileCount = maxFileCount;
        this.maxDepth = maxDepth;
        this.fileNameFilter = fileNameFilter;
        this.fileTypeFilter = fileTypeFilter;
        this.scanDate = LocalDate.now();
        this.scanTime = null;
        this.files = new HashSet<>();
        this.directoryPath = directoryPath;    
        this.strategyName = strategyName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "max_file_count")
    private Integer maxFileCount;

    @Column(name = "max_depth")
    private Integer maxDepth;

    @Column(name = "file_name_filter")
    private String fileNameFilter;

    @Column(name = "file_type_filter")
    private String fileTypeFilter;

    @Column(name = "scan_date")
    private LocalDate scanDate;

    @Column(name = "scan_time")
    private Long scanTime;

    @Column(name = "directory_path")
    private String directoryPath;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable(name = "FILE_SCAN_MAPPING", joinColumns = @JoinColumn(name = "scan_id"), inverseJoinColumns = @JoinColumn(name = "file_id"))
    private Set<FileInfo> files;
    
    @Column(name = "strategy_name")
    private ScannerStrategyName strategyName;

}
