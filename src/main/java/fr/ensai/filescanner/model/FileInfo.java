package fr.ensai.filescanner.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "file_information")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileInfo {

    public FileInfo(String fileName, LocalDateTime modificationDate, Long fileSize, String parentDirectory){
        this.id = null;
        this.fileName = fileName;
        this.modificationDate = modificationDate;
        this.fileSize = fileSize;
        this.parentDirectory = parentDirectory;
        this.scans = new HashSet<>();
        this.fileType = extractFileType(fileName);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "modification_date")
    private LocalDateTime modificationDate;

    @Column(name = "file_size")
    private Long fileSize;

    @Column(name = "file_type")
    private String fileType;

    @Column(name = "parent_directory")
    private String parentDirectory;

    @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH })
    @JoinTable(name = "FILE_SCAN_MAPPING", joinColumns = @JoinColumn(name = "file_id"), inverseJoinColumns = @JoinColumn(name = "scan_id"))
    private Set<Scan> scans;

    
    /** 
     * @param fileName
     * @return String
     */
    @Transient
    private String extractFileType(String fileName){
        int dotIndex = fileName.lastIndexOf('.');
        if (dotIndex != -1){
            return fileName.substring(dotIndex);
        }
        return "";
    }
}
