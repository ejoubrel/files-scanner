package fr.ensai.filescanner.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.google.common.collect.Iterables;

import fr.ensai.filescanner.dto.FileInfoOutDto;
import fr.ensai.filescanner.dto.ScanInDto;
import fr.ensai.filescanner.dto.ScanOutDto;
import fr.ensai.filescanner.dto.ScanOutFullDto;
import fr.ensai.filescanner.dto.ScanParametersDto;
import fr.ensai.filescanner.model.FileInfo;
import fr.ensai.filescanner.model.Scan;
import fr.ensai.filescanner.repository.FileInfoRepository;
import fr.ensai.filescanner.repository.ScanRepository;
import fr.ensai.filescanner.scanner.ScannerStrategy;
import fr.ensai.filescanner.scanner.ScannerStrategyFactory;

@Service
public class ScanService {
    
    Logger LOG = LoggerFactory.getLogger(ScanService.class);

    @Autowired
    private ScanRepository scanRepository;

    @Autowired
    private FileInfoRepository fileInfoRepository;

    @Autowired
    private ScannerStrategyFactory strategyFactory;

    
    /** 
     * @param id
     * @return ScanOutFullDto
     */
    public ScanOutFullDto getScan(final Long id) {
        Optional<Scan> scan = scanRepository.findById(id);
        if (!scan.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        Set<FileInfoOutDto> fileDtos = new HashSet<FileInfoOutDto>();
        Iterable<FileInfo> files = fileInfoRepository.findByScans(scan.get());
        files.forEach(
            file -> fileDtos.add(new FileInfoOutDto(file.getId(),
                                                    file.getFileName(),
                                                    file.getModificationDate(),
                                                    file.getFileSize(),
                                                    file.getFileType(),
                                                    file.getParentDirectory())));
        ScanOutFullDto dto = new ScanOutFullDto(scan.get().getId(),
                                                scan.get().getDirectoryPath(),
                                                scan.get().getMaxFileCount(),
                                                scan.get().getMaxDepth(),
                                                scan.get().getFileNameFilter(),
                                                scan.get().getFileTypeFilter(),
                                                scan.get().getScanDate(),
                                                scan.get().getScanTime(),
                                                fileDtos);
        return dto;
    }

    
    /** 
     * @return Iterable<ScanOutDto>
     */
    public Iterable<ScanOutDto> getScans() {
        Iterable<Scan> scans = scanRepository.findAll();
        List<ScanOutDto> dtos = new ArrayList<ScanOutDto>();
        scans.forEach(scan -> dtos.add(new ScanOutDto(scan.getId(),
                                                      scan.getDirectoryPath(),
                                                      scan.getMaxFileCount(),
                                                      scan.getMaxDepth(),
                                                      scan.getFileNameFilter(),
                                                      scan.getFileTypeFilter(),
                                                      scan.getScanDate(),
                                                      scan.getScanTime())));
        return dtos;
    }

    
    /** 
     * @param id
     * @return boolean
     */
    public boolean deleteScan(final Long id) {
        Optional<Scan> scan = scanRepository.findById(id);
        scanRepository.deleteById(id);
        return scan.isPresent();
    }

    
    /** 
     * @param inDto
     * @return ScanOutFullDto
     */
    public ScanOutFullDto saveScan(ScanInDto inDto) {
        Scan scan = new Scan(inDto.getDirectoryPath(),
                             inDto.getMaxFileCount(),
                             inDto.getMaxDepth(),
                             inDto.getFileNameFilter(),
                             inDto.getFileTypeFilter(),
                             inDto.getStrategyName());
        ScannerStrategy strategy = strategyFactory.findStrategy(inDto.getStrategyName());
        long start = System.nanoTime();
        List<FileInfo> files = strategy.scanDirectory(inDto.getDirectoryPath(),
                                             inDto.getFileNameFilter(),
                                             inDto.getFileTypeFilter(),
                                             inDto.getMaxDepth(),
                                             inDto.getMaxFileCount());
        long end = System.nanoTime();
        scan.setFiles(new HashSet<FileInfo>(files));
        scan.setScanTime(end-start);
        Scan savedScan = scanRepository.save(scan);
        Set<FileInfoOutDto> fileDtos = new HashSet<FileInfoOutDto>();
        savedScan.getFiles().forEach(
            file -> fileDtos.add(new FileInfoOutDto(file.getId(),
                                                    file.getFileName(),
                                                    file.getModificationDate(),
                                                    file.getFileSize(),
                                                    file.getFileType(),
                                                    file.getParentDirectory())));
        ScanOutFullDto dto = new ScanOutFullDto(savedScan.getId(),
                                                savedScan.getDirectoryPath(),
                                                savedScan.getMaxFileCount(),
                                                savedScan.getMaxDepth(),
                                                savedScan.getFileNameFilter(),
                                                savedScan.getFileTypeFilter(),
                                                savedScan.getScanDate(),
                                                savedScan.getScanTime(),
                                                fileDtos);
        return dto;
    }

    
    /** 
     * @param id
     * @param scanParameters
     * @return ScanOutFullDto
     */
    public ScanOutFullDto duplicateScan(Long id, ScanParametersDto scanParameters){
        Optional<Scan> initialScan = scanRepository.findById(id);
        if (!initialScan.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        boolean allNull = scanParameters.getFileNameFilter() == null
                          && scanParameters.getFileTypeFilter() == null
                          && scanParameters.getMaxDepth() == null
                          && scanParameters.getMaxFileCount() == null;
        boolean allSame = scanParameters.getFileNameFilter() == initialScan.get().getFileNameFilter()
                          && scanParameters.getFileTypeFilter() == initialScan.get().getFileTypeFilter()
                          && scanParameters.getMaxDepth() == initialScan.get().getMaxDepth()
                          && scanParameters.getMaxFileCount() == initialScan.get().getMaxFileCount();

        if (allNull || allSame){
                return getScan(id);
        }
        String newNameFilter = scanParameters.getFileNameFilter() == null ? initialScan.get().getFileNameFilter() : scanParameters.getFileNameFilter();
        String newTypeFilter = scanParameters.getFileTypeFilter() == null ? initialScan.get().getFileTypeFilter() : scanParameters.getFileTypeFilter();
        Integer newDepth = scanParameters.getMaxDepth() == null ? initialScan.get().getMaxDepth() : scanParameters.getMaxDepth();
        Integer newFiles = scanParameters.getMaxFileCount() == null ? initialScan.get().getMaxFileCount() : scanParameters.getMaxFileCount();

        ScanInDto newScan = new ScanInDto(initialScan.get().getDirectoryPath(), initialScan.get().getStrategyName(), newFiles, newDepth, newNameFilter, newTypeFilter);
        ScanOutFullDto savedScan = saveScan(newScan);
        return savedScan;
    }

    
    /** 
     * @param id
     * @return ScanOutFullDto
     */
    public ScanOutFullDto replayScan(Long id){
        Optional<Scan> initialScan = scanRepository.findById(id);
        if (!initialScan.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        // Scan directory again, and set new files and scan time
        ScannerStrategy strategy = strategyFactory.findStrategy(initialScan.get().getStrategyName());
        long start = System.nanoTime();
        List<FileInfo> files = strategy.scanDirectory(initialScan.get().getDirectoryPath(),
                                             initialScan.get().getFileNameFilter(),
                                             initialScan.get().getFileTypeFilter(),
                                             initialScan.get().getMaxDepth(),
                                             initialScan.get().getMaxFileCount());
        long end = System.nanoTime();
        initialScan.get().setFiles(new HashSet<FileInfo>(files));
        initialScan.get().setScanTime(end-start);

        // Save updated scan in database
        Scan savedScan = scanRepository.save(initialScan.get());
        Set<FileInfoOutDto> fileDtos = new HashSet<FileInfoOutDto>();
        savedScan.getFiles().forEach(
            file -> fileDtos.add(new FileInfoOutDto(file.getId(),
                                                    file.getFileName(),
                                                    file.getModificationDate(),
                                                    file.getFileSize(),
                                                    file.getFileType(),
                                                    file.getParentDirectory())));
        ScanOutFullDto dto = new ScanOutFullDto(savedScan.getId(),
                                                savedScan.getDirectoryPath(),
                                                savedScan.getMaxFileCount(),
                                                savedScan.getMaxDepth(),
                                                savedScan.getFileNameFilter(),
                                                savedScan.getFileTypeFilter(),
                                                savedScan.getScanDate(),
                                                savedScan.getScanTime(),
                                                fileDtos);
        return dto;
    }

    
    /** 
     * @return double
     */
    public double computeMeanTimePerFile() {
        double totalFiles = Iterables.size(fileInfoRepository.findAll());
        Iterable<Scan> scans = scanRepository.findAll();
        double totalTime = 0.;
        // scans.forEach(scan -> totalFiles = totalFiles + Iterables.size(scan.getFiles()));
        for (Scan scan : scans){
            totalTime = totalTime + scan.getScanTime();
        }
        double mean = totalTime / totalFiles;
        double result = Math.round(mean * 100) / 100;
        return result;
    }

    
    /** 
     * @return double
     */
    public double computeMeanTimePerDir() {
        Iterable<FileInfo> files = fileInfoRepository.findAll();
        List<String> parentDirs = new ArrayList<>();
        for (FileInfo fileInfo : files) {
            if (! parentDirs.contains(fileInfo.getParentDirectory())){
                parentDirs.add(fileInfo.getParentDirectory());
            }
        }
        double totalDirectories = parentDirs.size();
        
        Iterable<Scan> scans = scanRepository.findAll();
        double totalTime = 0.;
        for (Scan scan : scans) {
            totalTime = totalTime + scan.getScanTime();
        }

        double mean = totalTime / totalDirectories;
        double result = Math.round(mean * 100) / 100;
        return result;
    }
}
