package fr.ensai.filescanner.dto;

import fr.ensai.filescanner.scanner.ScannerStrategyName;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ScanInDto {
    private String directoryPath;
    private ScannerStrategyName strategyName;
    private Integer maxFileCount;
    private Integer maxDepth;
    private String fileNameFilter;
    private String fileTypeFilter;
}
