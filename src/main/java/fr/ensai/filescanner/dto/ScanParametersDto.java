package fr.ensai.filescanner.dto;

import lombok.Getter;

@Getter
public class ScanParametersDto {
    private String fileNameFilter;
    private String fileTypeFilter;
    private Integer maxFileCount;
    private Integer maxDepth;
}
