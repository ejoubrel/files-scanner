package fr.ensai.filescanner.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ScanOutDto {
    private Long id;
    private String directoryPath;
    private Integer maxFileCount;
    private Integer maxDepth;
    private String fileNameFilter;
    private String fileTypeFilter;
    private LocalDate scanDate;
    private Long scanTime;
}
