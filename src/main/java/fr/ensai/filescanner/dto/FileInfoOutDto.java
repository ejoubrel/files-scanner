package fr.ensai.filescanner.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class FileInfoOutDto {
    
    private Long id;
    private String filename;
    private LocalDateTime modificationDate;
    private Long fileSize;
    private String fileType;
    private String parentDirectory;
}
