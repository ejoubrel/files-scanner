package fr.ensai.filescanner.scanner;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import fr.ensai.filescanner.model.FileInfo;

@Component
public class S3BucketStrategy implements ScannerStrategy{
    
    
    /** 
     * @param bucketName
     * @param fileNameFilter
     * @param fileTypeFilter
     * @param maxDepth
     * @param maxFiles
     * @return List<FileInfo>
     */
    @Override
    public List<FileInfo> scanDirectory(String bucketName,
                                        String fileNameFilter,
                                        String fileTypeFilter,
                                        Integer maxDepth,
                                        Integer maxFiles) {
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
                .build();
        
        List<FileInfo> result = new ArrayList<>();

        ListObjectsV2Request request = new ListObjectsV2Request().withBucketName(bucketName);

        ListObjectsV2Result s3Objects;
        do {
            s3Objects = s3Client.listObjectsV2(request);

            for (S3ObjectSummary objectSummary : s3Objects.getObjectSummaries()){
                if (result.size() >= maxFiles){
                    return result;
                }

                String fileName = objectSummary.getKey();
                if (fileMatchesFilters(fileName, fileNameFilter, fileTypeFilter)) {
                    Instant instant = objectSummary.getLastModified().toInstant();
                    LocalDateTime modifiedDateTime = instant.atZone(ZoneId.systemDefault()).toLocalDateTime();

                    FileInfo fileInfo = new FileInfo(fileName, modifiedDateTime, objectSummary.getSize(), objectSummary.getBucketName());
                    result.add(fileInfo);
                }
            }
            request.setContinuationToken(s3Objects.getNextContinuationToken());
        } while (s3Objects.isTruncated());

        return result;
    }
    
    
    /** 
     * @param fileName
     * @param fileNameFilter
     * @param fileTypeFilter
     * @return boolean
     */
    private boolean fileMatchesFilters(String fileName, String fileNameFilter, String fileTypeFilter) {
        if (fileNameFilter != null && !fileName.contains(fileNameFilter)){
            return false;
        }
        if (fileTypeFilter != null && !fileName.endsWith(fileTypeFilter)){
            return false;
        }

        return true;
    }

    
    /** 
     * @return ScannerStrategyName
     */
    @Override
    public ScannerStrategyName getStrategyName(){
        return ScannerStrategyName.S3Bucket;
    }
}
