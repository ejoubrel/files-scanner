package fr.ensai.filescanner.scanner;

public enum ScannerStrategyName {
    Local,
    S3Bucket
}
