package fr.ensai.filescanner.scanner;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ScannerStrategyFactory {
    
    private Map<ScannerStrategyName, ScannerStrategy> strategies;

    @Autowired
    public ScannerStrategyFactory(Set<ScannerStrategy> strategySet){
        createStrategy(strategySet);
    }

    
    /** 
     * @param strategyName
     * @return ScannerStrategy
     */
    public ScannerStrategy findStrategy(ScannerStrategyName strategyName){
        return strategies.get(strategyName);
    }

    
    /** 
     * @param strategySet
     */
    private void createStrategy(Set<ScannerStrategy> strategySet){
        strategies = new HashMap<ScannerStrategyName, ScannerStrategy>();
        strategySet.forEach(
            strategy -> strategies.put(strategy.getStrategyName(), strategy)
        );
    }
}
