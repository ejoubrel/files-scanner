package fr.ensai.filescanner.scanner;

import java.io.File;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import fr.ensai.filescanner.model.FileInfo;

@Component
public class LocalStrategy implements ScannerStrategy{

    
    /** 
     * @param directoryPath
     * @param fileNameFilter
     * @param fileTypeFilter
     * @param maxDepth
     * @param maxFiles
     * @return List<FileInfo>
     */
    @Override
    public List<FileInfo> scanDirectory(String directoryPath,
                                        String fileNameFilter,
                                        String fileTypeFilter,
                                        Integer maxDepth,
                                        Integer maxFiles) {
        File directory = new File(directoryPath);
        int depth = maxDepth == null ? 1 : maxDepth.intValue();
        int files = maxFiles == null ? 50 : maxFiles.intValue();
        List<FileInfo> result = new ArrayList<>();
        scanDirectoryRecursive(directory, fileNameFilter, fileTypeFilter, files, depth, 0, result);
        return result;
    }

    
    /** 
     * @param directory
     * @param fileNameFilter
     * @param fileTypeFilter
     * @param maxFiles
     * @param maxDepth
     * @param currentDepth
     * @param result
     */
    private void scanDirectoryRecursive(File directory,
                                        String fileNameFilter,
                                        String fileTypeFilter,
                                        int maxFiles,
                                        int maxDepth,
                                        int currentDepth,
                                        List<FileInfo> result) {
            if (currentDepth > maxDepth || result.size() >= maxFiles){
                return;
            }

            if (directory.isDirectory()) {
                File[] files = directory.listFiles();
                if (files != null) {
                    for (File file : files){
                        if (file.isFile() && fileMatchesFilters(file, fileNameFilter, fileTypeFilter)) {
                            LocalDateTime modificationDateTime = Instant.ofEpochMilli(file.lastModified())
                                                                    .atZone(ZoneId.systemDefault())
                                                                    .toLocalDateTime();
                            FileInfo fileInfo = new FileInfo(file.getName(), modificationDateTime, file.length(), file.getParent());
                            result.add(fileInfo);
                        }
                        if (file.isDirectory()) {
                            scanDirectoryRecursive(file,
                                                   fileNameFilter,
                                                   fileTypeFilter,
                                                   maxFiles,
                                                   maxDepth,
                                                   currentDepth + 1,
                                                   result);
                        }
                        if (result.size() >= maxFiles){
                            return;
                        }
                    }
                }
            }
        }

    
    /** 
     * @param file
     * @param fileNameFilter
     * @param fileTypeFilter
     * @return boolean
     */
    private boolean fileMatchesFilters(File file, String fileNameFilter, String fileTypeFilter) {
        if (fileNameFilter != null && !file.getName().contains(fileNameFilter)) {
            return false;
        }

        if (fileTypeFilter != null && !file.getName().endsWith(fileTypeFilter)) {
            return false;
        }

        return true;
    }

    
    /** 
     * @return ScannerStrategyName
     */
    @Override
    public ScannerStrategyName getStrategyName(){
        return ScannerStrategyName.Local;
    }
}
