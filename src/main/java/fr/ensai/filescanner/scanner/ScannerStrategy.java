package fr.ensai.filescanner.scanner;

import java.util.List;

import fr.ensai.filescanner.model.FileInfo;

public interface ScannerStrategy {
    List<FileInfo> scanDirectory(String directoryPath,
                                 String fileNameFilter,
                                 String FileTypeFilter,
                                 Integer maxDepth,
                                 Integer maxFiles);
    
    ScannerStrategyName getStrategyName();
}
