package fr.ensai.filescanner.repository;

import java.time.LocalDateTime;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.ensai.filescanner.model.Scan;

@Repository
public interface ScanRepository extends CrudRepository<Scan, Long> {
    public Iterable<Scan> findByScanDate(LocalDateTime scanDate);
}
