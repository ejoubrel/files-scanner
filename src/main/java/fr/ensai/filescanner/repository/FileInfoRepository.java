package fr.ensai.filescanner.repository;

import org.springframework.data.repository.CrudRepository;

import fr.ensai.filescanner.model.FileInfo;
import fr.ensai.filescanner.model.Scan;

public interface FileInfoRepository extends CrudRepository<FileInfo, Long> {
    public Iterable<FileInfo> findByFileName(String fileName);
    public Iterable<FileInfo> findByScans(Scan scan);
}
