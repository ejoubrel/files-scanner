# Project Setup and Execution Guide

## Prerequisites
- Java 17 needs to be installed on your system.

### For Ubuntu
```bash
apt install openjdk-17-jdk openjdk-17-jre
```

### For macOS
Follow the instructions on the [official Oracle documentation](https://docs.oracle.com/en/java/javase/17/install/installation-jdk-macos.html#GUID-2FE451B0-9572-4E38-A1A5-568B77B146DE).

## Java Verification
To confirm Java is correctly installed, run:
```bash
java -version
```

## Project Setup
1. **Clone the Git repository**: Clone the repository.
2. **Navigate to the project directory**: Move to the directory where the project was cloned.

## Running the project locally
After selecting the good properties in application.properties, use the Maven Wrapper included in the project to launch the Spring Boot application:
```bash
./mvnw spring-boot:run
```

## Docker (Optional)
If the project is dockerized, build and run the Docker container:
```bash
docker run --name some-postgres -e POSTGRES_DB=db_demo -e POSTGRES_USER=db_user -e POSTGRES_PASSWORD=db_password -p 5433:5432 -d postgres
docker build -t your-application-name .
docker run -p 8080:8080 your-application-name
```

## Testing
Run unit and integration tests with Maven:
```bash
./mvnw test
```

## CI/CD
CI/CD configurations are defined in `.gitlab-ci.yml` for continuous integration and deployment.